﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretEnemyScript : MonoBehaviour
{

    private Transform playerPos;
    //[SerializeField] private AnimationsController ac;
    [SerializeField] private float maxDistance;
    [SerializeField] private Rigidbody bullet;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private int numShoots;
    [SerializeField] private GameObject muzzleFlash;

    [SerializeField] private Transform firePoint;
    private Vector3 lastPosition = Vector3.zero;
    private Vector3 shootVector;
    private Vector3 angle;
    private Quaternion targetRotation;


    private bool canFire;
    private bool firstTime = true;
    private float fireCooldown;

    [SerializeField] private float startFireCooldown;
    [SerializeField] private float spread;

    //public AudioClip wepSoundClip;
    //private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        canFire = false;
        fireCooldown = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (fireCooldown <= 0)
        {
            canFire = true;
            
        }
        else
        {
            fireCooldown -= Time.fixedDeltaTime;
        }

        if (Vector3.Distance(playerPos.position, gameObject.transform.position) <= maxDistance)
        {
            //if (gameObject.GetComponent<EnemyMinion>().enemyHealth > 0)
            //{
                transform.LookAt(playerPos);
                var rand = Random.Range(-spread, spread);
                //implement random ranges on playerPos.transform x, y, z;
                lastPosition = new Vector3(playerPos.transform.position.x + rand, (playerPos.transform.position.y + rand), playerPos.transform.position.z);
                shootVector = lastPosition - transform.position;
                angle = Quaternion.Euler(0, 180, 0) * shootVector;
                targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: angle);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 2 * Time.fixedDeltaTime);

                ShootPlayer();
            //}

        }
    }

    private void ShootPlayer()
    {
        //ac.SetMovingState(false);
        if (canFire)
        {
            //ac.Attack();
            //audioSource.Play();
            if (firstTime) {

                StartCoroutine(Shooting());
                firstTime = false;

            }


        }

        
    }

    IEnumerator Shooting()
    {
        for(int i = 0; i < numShoots; i++){

            Rigidbody instance = Instantiate(bullet, firePoint.position, firePoint.rotation);
            GameObject particle = Instantiate(muzzleFlash, firePoint.position, firePoint.rotation);
            particle.transform.parent = gameObject.transform;

            shootVector.Normalize();
            instance.velocity = shootVector * bulletSpeed;

            yield return new WaitForSeconds(.5f);
        }

        firstTime = true;
    
        fireCooldown = startFireCooldown;
        canFire = false;
        
        
    }
}
