﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManagerFunctional : MonoBehaviour
{
    [SerializeField] private Slider sliderMaster;
    [SerializeField] private Slider sliderFx;
    [SerializeField] private Slider sliderEnviroment;

    public AudioMixer mixerMaster;


    // Start is called before the first frame update

    
    void Start()
    {
        AssingValuesFromPlayerPref();
    }

    private void AssingValuesFromPlayerPref()
    {
        sliderMaster.value += PlayerPrefs.GetFloat("MasterVolume");
        sliderFx.value += PlayerPrefs.GetFloat("FxVolume");
        sliderEnviroment.value += PlayerPrefs.GetFloat("EnviromentalVolume");

        if (PlayerPrefs.GetFloat("MasterVolume") != 0) {

            mixerMaster.SetFloat("Master", PlayerPrefs.GetFloat("MasterVolume"));
            mixerMaster.SetFloat("Fx", PlayerPrefs.GetFloat("FxVolume"));
            mixerMaster.SetFloat("Enviromental", PlayerPrefs.GetFloat("EnviromentalVolume"));
        }
    }
   

}
