﻿using UnityEngine;

public class EggController : Enemy, IDamageable
{
    [SerializeField] GameObject _full;
    [SerializeField] GameObject _damaged;
    [SerializeField] private ParticleSystem _particleSystem;


    public void Damage(float damage)
    {
        enemyHealth -= damage;

        if (enemyHealth <= 0)
        {            
            StartCoroutine(CocoonDeath(_particleSystem));
        }
    }

    /*
    public void Clear()
    {
        //_full.gameObject.SetActive(true);
        //_damaged.gameObject.SetActive(false); 
    }
    */

    public void Destroy()
    {
        //_full.gameObject.SetActive(false);
        //_damaged.gameObject.SetActive(true);
        CocoonDeath(_particleSystem);
    }

}