﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadlightScript : MonoBehaviour
{
    [SerializeField] private GearLever[] levers;
    [SerializeField] private GameObject leftLight;
    [SerializeField] private GameObject rightLight;
    [SerializeField] private float lightShortIntensity;
    [SerializeField] private float lightLongIntensity;
    [SerializeField] private float lightShortRange;
    [SerializeField] private float lightLongRange;

    void Awake()
    {
        levers[0].leverState = false;
        levers[1].leverState = false;
        leftLight.SetActive(false);
        rightLight.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.currentState == GameManager.temperatureStates.overheat)
        {
            if (levers[1].leverState)
            {
                levers[1].OnClick();
            }
            if (levers[0].leverState)
            {
                levers[0].OnClick();
            }
            /*
            leftLight.SetActive(false);
            rightLight.SetActive(false);*/
            Debug.Log("LIGHTOFF");
        }
        if (levers[0].leverState && !levers[1].leverState)
        {
            leftLight.SetActive(true);
            rightLight.SetActive(true);
            leftLight.GetComponent<Light>().intensity = lightShortIntensity;
            rightLight.GetComponent<Light>().intensity = lightShortIntensity;
            leftLight.GetComponent<Light>().range = lightShortRange;
            rightLight.GetComponent<Light>().range = lightShortRange;
        }
        else if (levers[1].leverState && !levers[0].leverState)
        {
            leftLight.SetActive(true);
            rightLight.SetActive(true);
            leftLight.GetComponent<Light>().intensity = lightLongIntensity;
            rightLight.GetComponent<Light>().intensity = lightLongIntensity;
            leftLight.GetComponent<Light>().range = lightLongRange;
            rightLight.GetComponent<Light>().range = lightLongRange;
            if (GameManager.Instance.currentState == GameManager.temperatureStates.increaseTemp || GameManager.Instance.currentState == GameManager.temperatureStates.decreaseTemp)
            {
                GameManager.Instance.increaseTemp(0.5f * Time.deltaTime);
            }
        }
        else
        {
            leftLight.SetActive(false);
            rightLight.SetActive(false);
        }
    }
}
