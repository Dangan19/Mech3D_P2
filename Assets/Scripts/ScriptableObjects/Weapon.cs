﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Weapon", menuName = "WeponData", order = 1)]
public class Weapon : ScriptableObject
{
    public string weaponName;
    public int id;

    //bullet & bullet stats
    public GameObject bulletType;
    public float shootForce;
    public float upwardForce;
    public float damage;
    
    //Gun stats
    public float timeBetweenShooting;
    public float timeBetweenShots;
    public float spread;
    public float realoadTime;
    public int  bulletsPerTap;
    public int   magazineSize;
    public bool allowButtonHold;

    //Recoil
    public float recoilForce;

    //Prefab
    public GameObject PhysicalWeapon;

    //audioClip
    public AudioClip shootingSound;

    //Sprite(Garage)
    public Sprite weaponImg;
}
