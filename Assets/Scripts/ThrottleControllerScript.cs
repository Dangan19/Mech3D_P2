﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrottleControllerScript : MonoBehaviour, IClickable
{
    private Animator anim;

    private void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    public void OnClick()
    {
        Debug.Log("IT WORKS");
        //anim.Play("ThrottleControler");
         

        if (anim.GetBool("Click"))
        {
            anim.SetBool("Click", false);
        }
        else
        {
            anim.SetBool("Click", true);
        }
    }
}
