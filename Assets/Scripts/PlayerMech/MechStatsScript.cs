﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechStatsScript : MonoBehaviour
{
    [SerializeField] private GameObject LeftHandLight;
    [SerializeField] private GameObject RightHandLight;
    [SerializeField] private GameObject LeftLegLight;
    [SerializeField] private GameObject RightLegLight;
    [SerializeField] private GameObject CockpitLight;

    private Renderer LeftHandRender;
    private Renderer RightHandRender;
    private Renderer LeftLegRender;
    private Renderer RightLegRender;
    private Renderer CockpitRender;

    private float LeftWeaponMaxHp;
    private float RightWeaponMaxHp;
    private float LeftLegMaxHp;
    private float RightLegMaxHp;
    private float CockpitMaxHp;

    //private Color orange;

    void Start()
    {
        //orange = new Color(1, 0.5f, 0f, 1);
        LeftWeaponMaxHp = HealthManagerScript.Instance.LeftWeapon;
        RightWeaponMaxHp = HealthManagerScript.Instance.RightWeapon;
        LeftLegMaxHp = HealthManagerScript.Instance.LeftLeg;
        RightLegMaxHp = HealthManagerScript.Instance.RightLeg;
        CockpitMaxHp = HealthManagerScript.Instance.CockPit;

        LeftHandRender = LeftHandLight.GetComponent<Renderer>();
        RightHandRender = RightHandLight.GetComponent<Renderer>();
        LeftLegRender = LeftLegLight.GetComponent<Renderer>();
        RightLegRender = RightLegLight.GetComponent<Renderer>();
        CockpitRender = CockpitLight.GetComponent<Renderer>();

        SetAllLightsGreen();

    }

    void Update()
    {
        CheckParts();
    }

    void SetAllLightsGreen()
    {
        LeftHandRender.material.SetColor("_Color", Color.green);
        RightHandRender.material.SetColor("_Color", Color.green);
        LeftLegRender.material.SetColor("_Color", Color.green);
        RightLegRender.material.SetColor("_Color", Color.green);
        CockpitRender.material.SetColor("_Color", Color.green);
    }

    void SetLights(Color c, Renderer mechPart)
    {
        mechPart.material.SetColor("_Color", c);
        mechPart.material.SetColor("_EmissionColor", c);
    }

    void CheckParts()
    {
        if (HealthManagerScript.Instance.LeftWeapon <= LeftWeaponMaxHp * 0.5) SetLights(Color.red, LeftHandRender);
        if (HealthManagerScript.Instance.RightWeapon <= RightWeaponMaxHp * 0.5) SetLights(Color.red, RightHandRender);
        if (HealthManagerScript.Instance.LeftLeg <= LeftLegMaxHp * 0.5) SetLights(Color.red, LeftLegRender);
        if (HealthManagerScript.Instance.RightLeg <= RightLegMaxHp * 0.5) SetLights(Color.red, RightLegRender);
        if (HealthManagerScript.Instance.CockPit <= CockpitMaxHp * 0.5) SetLights(Color.red, CockpitRender);

        if (HealthManagerScript.Instance.LeftWeapon <= 0) SetLights(Color.clear, LeftHandRender);    
        if (HealthManagerScript.Instance.RightWeapon <= 0) SetLights(Color.clear, RightHandRender);
        if (HealthManagerScript.Instance.LeftLeg <= 0) SetLights(Color.clear, LeftLegRender);
        if (HealthManagerScript.Instance.RightLeg <= 0) SetLights(Color.clear, RightLegRender);
        if (HealthManagerScript.Instance.CockPit <= 0) SetLights(Color.clear, CockpitRender);

        //onDamagePart(LeftHandRender);
    }

    public void onDamagePart(String mechPartName)
    {
        Debug.Log("dmg on this part: " + mechPartName);
        switch (mechPartName)
        {
            case "RightLegCollider":
                StartCoroutine(Damage(RightLegRender));
                break;

            case "LeftLegCollider":
                StartCoroutine(Damage(LeftLegRender));
                break;

            case "LeftWeaponCollider":
                StartCoroutine(Damage(LeftHandRender));
                break;

            case "RightWeaponCollider":
                StartCoroutine(Damage(RightHandRender));
                break;

            case "CabinaCollider":
                StartCoroutine(Damage(CockpitRender));
                break;
        }

    }

    IEnumerator Damage(Renderer mechPart)
    {
        mechPart.enabled = false;
        yield return new WaitForSeconds(.25f);
        mechPart.enabled = true;
        yield return new WaitForSeconds(.25f);
        mechPart.enabled = false;
        yield return new WaitForSeconds(.25f);
        mechPart.enabled = true;
    }

    //private void SetColorBlocks(Color c)
    //{
    //    for (int i = 0; i < blockList.Length; i++)
    //    {
    //        blockList[i].GetComponent<Renderer>().material.SetColor("_Color", c);
    //    }

    //}
}
