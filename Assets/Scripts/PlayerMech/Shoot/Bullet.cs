﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.PlayerMech.Shoot
{
    public class Bullet : MonoBehaviour
    {
        private float damage;
        [SerializeField] private float timeTolife;
        public GameObject collisionParticleBlood;

        public void SetDamage(float d)
        {
            damage = d;
        }

        public virtual void Start()
        {
            Destroy(gameObject, timeTolife);
        }
        

        public void CheckIfdamageable(GameObject collisionObject, float angle)
        {
            IDamageable idamageable = collisionObject.GetComponent<IDamageable>();
            if (idamageable != null)
            {
                Instantiate(collisionParticleBlood, transform.position, Quaternion.Euler(0, angle, 0));
                idamageable.Damage(damage);
                Destroy(gameObject);
            }
        }
    }
}