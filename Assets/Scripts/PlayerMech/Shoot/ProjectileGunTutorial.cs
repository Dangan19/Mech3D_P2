﻿using UnityEngine;
using TMPro;
using Assets.Scripts.PlayerMech.Shoot;

public class ProjectileGunTutorial : MonoBehaviour
{
    public Weapon weapon;
    public GameObject wp;
    public AudioSource audioSourceReload;

    //bullet 
    public GameObject bullet;
    int bulletsLeft, bulletsShot;
    public int maxBullets;

    //Recoil
    public Rigidbody playerRb;

    //bools
    bool shooting, readyToShoot, reloading;

    //Reference
    public Camera fpsCam;
    public Transform attackPoint;

    //Graphics
    public GameObject muzzleFlash;

    //bug fixing :D
    public bool allowInvoke = true;

    //Arm
    public bool leftHand;

    public delegate void FinishReload();
    public event FinishReload OnFinish;


    public delegate void ReloadStart();
    public event ReloadStart OnReload;

    private AudioSource audioSource;

    private void Start()
    {
        if (leftHand)
        {
            weapon = GameManager.Instance.leftWeapon;
        }
        else
        {
            weapon = GameManager.Instance.rightWeapon;
        }

        //make sure magazine is full
        bulletsLeft = weapon.magazineSize;
        maxBullets = weapon.magazineSize;
        bullet = weapon.bulletType;
        readyToShoot = true;

        ChangeWeapon();

        attackPoint = wp.transform.GetChild(1).gameObject.transform;

        audioSource = GetComponent<AudioSource>();
        audioSource.clip = weapon.shootingSound;

    }

    private void Update()
    {
        MyInput();
        
    }
    private void MyInput()
    {
        //Check if allowed to hold down button and take corresponding input

        if (leftHand) {

            if (weapon.allowButtonHold) shooting = Input.GetButton("Fire1");
            else shooting = Input.GetButtonDown("Fire1");
        }
        else
        {
            if (weapon.allowButtonHold) shooting = Input.GetButton("Fire2");
            else shooting = Input.GetButtonDown("Fire2");
        }

        //Reloading 
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < weapon.magazineSize && !reloading) Reload();
        //Reload automatically when trying to shoot without ammo
        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0) Reload();

        //Shooting
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0 && GameManager.Instance.currentState != GameManager.temperatureStates.overheat)
        {
            //Set bullets shot to 0
            bulletsShot = 0;
            audioSource.Play();

            Shoot();
        }
    }
    private void Shoot()
    {
        readyToShoot = false;

        //Find the exact hit position using a raycast
        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 00f)); //Just a ray through the middle of your current view
        RaycastHit hit;

        //check if ray hits something
        Vector3 targetPoint;

        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log("hit raycastttttttttttttttt" + hit.collider.name);

            targetPoint = hit.point;
        }
        else
            targetPoint = ray.GetPoint(75); //Just a point far away from the player

        //Calculate direction from attackPoint to targetPoint
        Vector3 directionWithoutspread = targetPoint - attackPoint.position;
        float distance = Vector3.Distance(targetPoint, attackPoint.position);
        //Debug.Log("direccio "+directionWithoutspread);
        Debug.Log("Distance: "+ distance);
        /*if (distance < 7)
        {
            Vector3 normalizedVec = directionWithoutspread.normalized;
            normalizedVec *= 7;
            targetPoint += normalizedVec;
            Debug.Log("In min distance");
        }*/
        

        //Calculate weapon.spread
        float x = Random.Range(-weapon.spread, weapon.spread);
        float y = Random.Range(-weapon.spread, weapon.spread);

        //Calculate new direction with weapon.spread
        Vector3 directionWithspread = directionWithoutspread + new Vector3(x, y, 0); //Just add weapon.spread to last direction

        //Instantiate bullet/projectile
        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity); //store instantiated bullet in currentBullet       

        //Rotate bullet to shoot direction
        currentBullet.transform.forward = directionWithspread.normalized;

        //Add forces to bullet
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithspread.normalized * weapon.shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * weapon.upwardForce, ForceMode.Impulse);

        
        if (currentBullet.GetComponent(typeof(MissileScript)))
        {
            ((MissileScript)currentBullet.GetComponent(typeof(MissileScript))).SetTarget(targetPoint);
        }
        

        //Add damage to the bullet
        if (HealthManagerScript.Instance.isDamageState)
        {
            ((Bullet)currentBullet.GetComponent(typeof(Bullet))).SetDamage(weapon.damage * HealthManagerScript.Instance.damageModifier);
        }
        else
        {
            ((Bullet)currentBullet.GetComponent(typeof(Bullet))).SetDamage(weapon.damage);
        }

        

        //Add size to the bullet
        float size = weapon.damage / 2;
        if (size > 0.5f) size = 0.5f;
        currentBullet.transform.localScale *= 1 + size;

        //Instantiate muzzle flash, if you have one
        if (muzzleFlash != null)
        {
            GameObject muzzle = Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);
            muzzle.transform.parent = gameObject.transform;
        }

        bulletsLeft--;
        bulletsShot++;


        //Invoke resetShot function (if not already invoked), with your weapon.timeBetweenShooting
        if (allowInvoke)
        {
            Invoke("ResetShot", weapon.timeBetweenShooting);
            allowInvoke = false;

            //Add recoil to player (should only be called once)
            playerRb.AddForce(-directionWithspread.normalized * weapon.recoilForce, ForceMode.Impulse);
        }

        //if more than one weapon.bulletsPerTap make sure to repeat shoot function
        if (bulletsShot < weapon.bulletsPerTap && bulletsLeft > 0)
            Invoke("Shoot", weapon.timeBetweenShots);

        if (GameManager.Instance.currentState == GameManager.temperatureStates.increaseTemp || GameManager.Instance.currentState == GameManager.temperatureStates.decreaseTemp)
        {
            GameManager.Instance.increaseTemp(0.25f);
        }
    }
    private void ResetShot()
    {
        //Allow shooting and invoking again
        readyToShoot = true;
        allowInvoke = true;
    }

    private void Reload()
    {
        OnReload();
        reloading = true;
        Invoke("ReloadFinished", weapon.realoadTime); //Invoke ReloadFinished function with your weapon.reloadTime as delay
    }
    private void ReloadFinished()
    {
        Debug.Log("reloaded finished");
        //Fill magazine
        bulletsLeft = weapon.magazineSize;
        reloading = false;
        OnFinish();
        audioSourceReload.Play();
    }

    private void ChangeWeapon()
    {
        wp = Instantiate(weapon.PhysicalWeapon, gameObject.transform);
    }

    public int GetMagazineSize()
    {

        return maxBullets;
    }

    public int GetCurrentBullets()
    {
        return bulletsLeft;
    }
}
