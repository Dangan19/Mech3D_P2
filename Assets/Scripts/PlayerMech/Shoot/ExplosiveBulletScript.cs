﻿using Assets.Scripts.PlayerMech.Shoot;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBulletScript : Bullet
{
    [SerializeField] private float _triggerForce;
    [SerializeField] private float _explosionRadius;
    [SerializeField] private float _explosionForce;
    [SerializeField] private GameObject explosionParticles;

    //Audio
    protected AudioSource audioSource;
    [SerializeField] private AudioClip audioClipExplosion;

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        audioSource.clip = audioClipExplosion;        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude >= _triggerForce)
        {
            var surroundingObjects = Physics.OverlapSphere(transform.position, _explosionRadius);


            foreach (var obj in surroundingObjects)
            {
                Debug.Log("Object found " + obj);
                var rb = obj.GetComponent<Rigidbody>();
                if (rb == null) continue;

                CheckIfdamageable(obj.gameObject, 0);
                rb.AddExplosionForce(_explosionForce, transform.position, _explosionRadius);                
                
            }

            Instantiate(explosionParticles, transform.position, Quaternion.identity);
            audioSource.Play();

            Destroy(gameObject);
        }
    }
}
