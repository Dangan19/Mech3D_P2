﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileScript : ExplosiveBulletScript
{

    [SerializeField] private float missileFlySpeed;
    [SerializeField] private float missileTurnSpeed;
    [SerializeField] private Vector3 accelerationVector;
    [SerializeField] private float acceleartion;
    [SerializeField] private float maxSpeed;
    [SerializeField] private ParticleSystem smoke;
    private bool isGuidedTime = false;
    private Vector3 missileTarget;

    private Transform missileLocalTrans;
    private Rigidbody missileRgb;


    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        audioSource = GetComponent<AudioSource>();
        missileLocalTrans = GetComponent<Transform>();
        missileRgb = GetComponent<Rigidbody>();

        Instantiate(smoke, transform.position, Quaternion.identity);

        StartCoroutine(Fly());
    }

    // Update is called once per frame
    void Update()
    {
        if (isGuidedTime)
        {
            missileRgb.velocity = missileLocalTrans.forward * missileFlySpeed;
            var missileTargetRotation = Quaternion.LookRotation(missileTarget - missileLocalTrans.position);
            missileRgb.MoveRotation(Quaternion.RotateTowards(missileLocalTrans.rotation, missileTargetRotation, missileTurnSpeed));
        }
        else
        {
            //missileRgb.AddRelativeForce(missileLocalTrans.position.normalized + Vector3.forward * acceleartion);

            if (missileRgb.velocity.magnitude <= maxSpeed)
            {
                missileRgb.AddRelativeForce(accelerationVector * acceleartion);
                /*
                var angle = Mathf.Acos(Vector2.Angle(Vector3.forward, accelerationVector));
                Debug.Log("angle: " + angle);

                missileLocalTrans.Rotate(new Vector3(0, 0, 0));
                */
            }

        }

        Debug.Log("not guided");
    }

    private IEnumerator Fly()
    {
        
        yield return new WaitForSeconds(1.5f);
        isGuidedTime = true;
    }

    public void SetTarget(Vector3 targetTransform)
    {
        Debug.Log("The missile target is " + targetTransform);
        missileTarget = targetTransform;
        
    }
}
