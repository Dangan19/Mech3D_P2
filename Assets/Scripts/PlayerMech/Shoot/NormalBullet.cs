﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.PlayerMech.Shoot
{
    public class NormalBullet : Bullet
    {
        public GameObject collisionParticleStone;

        private float angle;
        private Vector3 normal;


        private void OnCollisionEnter(Collision collision)
        {
            var point = collision.contacts[0].point;
            var dir = -collision.contacts[0].normal;

            point -= dir;
            RaycastHit hitInfo;

            if (collision.collider.Raycast(new Ray(point, dir), out hitInfo, 2))
            {
                // this is the collider surface normal
                normal = hitInfo.normal;
                // this is the collision angle
                // you might want to use .velocity instead of .forward here, but it 
                // looks like it's already changed due to bounce in OnCollisionEnter
                angle = Vector3.Angle(-transform.forward, normal);

            }

            if (collision.gameObject.CompareTag("Obstacle"))
            {
                Debug.Log("Obstacle collision");
                Instantiate(collisionParticleStone, transform.position, Quaternion.Euler(0, angle, 0));
                Destroy(gameObject);
            }
            else{

                CheckIfdamageable(collision.gameObject, angle);
            }

            /*
            IDamageable idamageable = collision.gameObject.GetComponent<IDamageable>();
            if (idamageable != null)
            {
                Instantiate(collisionParticleBlood, transform.position, Quaternion.Euler(0, angle, 0));
                idamageable.Damage(damage);
                Destroy(gameObject);
            }
            */

            
        }
    }
}