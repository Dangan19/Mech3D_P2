﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class HealthManagerScript : MonoBehaviour
{
    public static HealthManagerScript Instance { get; private set; }

    public Image fadeOut;

    [SerializeField] private int resultScreenID;

    //Body parts
    public float LeftWeapon;
    public float RightWeapon;
    public float CockPit;
    public float LeftLeg;
    public float RightLeg;

    //fuel mechanic
    public Transform trans;
    private Vector3 lastPosition = new Vector3(0, 0, 0);
    public float current = 10f;
    public float max = 10f;
    public float burnRate = 0.5f;

    //States mechanic
    public GameObject atackLight;
    public GameObject defenseLight;
    public GameObject mobilityLight;

    public bool isMobilityState = false;
    public float mobilityModifier;

    public bool isDefenseState = false;
    public float defenseModifier;

    public bool isDamageState = false;
    public float damageModifier;
    public float reloadModifier;

    public float debufo;
    private bool bothLegsDestroyed = false;
    private bool oneLegDestroyed = false;

    //Losing components mechanic
    public GameObject leftWeaponObject;
    public GameObject rightWeaponObject;
    public GameObject explosionPart;    
    public MechStatsScript mss;
    public GameObject explosion;

    //Visual damage
    public GameObject impactSiteOne;
    public GameObject sparks;
    public GameObject currentSpark;    
    public Image bloodPanel;
    public GameObject explosionDeath;    

    //Audio
    private AudioSource audioSource;
    [SerializeField]private AudioClip audioClipExplosion;
    [SerializeField] private AudioClip audioClipHit;

    private void Awake()
    {
        current = max;

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        audioSource = explosion.GetComponent<AudioSource>();
        resultScreenID = 0;
    }

    private void Update()
    {
        if (oneLegDestroyed)
        {
            debufo = 0.75f;
        }
        else if (bothLegsDestroyed)
        {
            debufo = 0.5f;
        }
        else
        {
            debufo = 1f;
        }

        ConsumeFuel();        
    }


    private void Death()
    {
                   
        if (CockPit <= 0)
        {
            Instantiate(explosionPart, explosionDeath.transform.position, Quaternion.identity);
            StartCoroutine(FadeOut());
        }

        if (LeftWeapon <= 0 && leftWeaponObject != null)
        {
            audioSource.clip = audioClipExplosion;
            explosion.transform.position = leftWeaponObject.transform.position;
            Instantiate(explosionPart, explosion.transform.position, Quaternion.identity);
            audioSource.Play();
            Destroy(leftWeaponObject);
        }

        if (RightWeapon <= 0 && rightWeaponObject != null)
        {
            audioSource.clip = audioClipExplosion;
            explosion.transform.position = rightWeaponObject.transform.position;
            Instantiate(explosionPart, explosion.transform.position, Quaternion.identity);
            audioSource.Play();
            Destroy(rightWeaponObject);
        }

        if (LeftLeg <= 0 && RightLeg <= 0)
        {
            bothLegsDestroyed = true;
        }

        if (LeftLeg <= 0 || RightLeg <= 0)
        {
            oneLegDestroyed = true;
        }
    }

    private IEnumerator FadeOut()
    {
        fadeOut.enabled = true;
        resultScreenID = 1;

        for (float i = 0; i <= 1; i += Time.deltaTime)
        {
            // set color with i as alpha
            fadeOut.color = new Color(0, 0, 0, i);
            yield return null;
        }

        PlayerPrefs.SetInt("resultID", resultScreenID);
        SceneManager.LoadScene("ResultScene");
    }
    

    public void OnDamage(float damage, string part)
    {
        audioSource.clip = audioClipHit;
        audioSource.Play();

        if (isDefenseState)
        {
            damage = damage / defenseModifier;
        }

        switch (part)
        {
            case "LeftWeaponCollider":
                LeftWeapon -= damage;
                mss.onDamagePart("LeftWeaponCollider");
                break;

            case "RightWeaponCollider":
                RightWeapon -= damage;
                mss.onDamagePart("RightWeaponCollider");
                break;

            case "CabinaCollider":
                CockPit -= damage;
                mss.onDamagePart("CabinaCollider");
                break;

            case "LeftLegCollider":
                LeftLeg -= damage;
                mss.onDamagePart("LeftLegCollider");
                break;

            case "RightLegCollider":
                RightLeg -= damage;
                mss.onDamagePart("RightLegCollider");
                break;

        }

        currentSpark = Instantiate(sparks, impactSiteOne.transform.position, Quaternion.identity);
        StartCoroutine(HitEffect());
        

        //StartCoroutine(_cameraShake.Shake(0.15f, 0.00000000001f));
        //GameObject.Find("Player").transform.parent = currentSpark.transform;

        Death();
    }

    public void ConsumeFuel()
    {
        if (lastPosition != trans.position)
        {
            current -= burnRate * Time.deltaTime;

            if (current <= 0f)
            {
                Death();
            }
        }

        lastPosition = trans.position;
        
    }

    public void SetBurnRate(float gas)
    {
        burnRate = gas;
    }

    public void StateButtonPressed(int butttonNum )
    {
        switch (butttonNum)
        {
            case 0:
                SetLightColor(atackLight, Color.red);
                SetLightColor(defenseLight, Color.red);
                SetLightColor(mobilityLight, Color.red);

                isMobilityState = false;
                isDefenseState = false;
                isDamageState = false;

                break;

            case 1:
                Debug.Log("Atack mode engaged");
                SetLightColor(atackLight, Color.green);
                SetLightColor(defenseLight, Color.red);
                SetLightColor(mobilityLight, Color.red);

                isMobilityState = false;
                isDefenseState = false;
                isDamageState = true;

                break;

            case 2:
                Debug.Log("Defense mode engaged");
                SetLightColor(atackLight, Color.red);
                SetLightColor(defenseLight, Color.green);
                SetLightColor(mobilityLight, Color.red);

                isMobilityState = false;
                isDefenseState = true;
                isDamageState = false;
                break;

            case 3:
                Debug.Log("Mobility mode engaged");
                SetLightColor(atackLight, Color.red);
                SetLightColor(defenseLight, Color.red);
                SetLightColor(mobilityLight, Color.green);

                isMobilityState = true;
                isDefenseState = false;
                isDamageState = false;

                if (GameManager.Instance.currentState == GameManager.temperatureStates.increaseTemp || GameManager.Instance.currentState == GameManager.temperatureStates.decreaseTemp)
                {
                    GameManager.Instance.increaseTemp(2 * Time.deltaTime);
                }
                break;
        }
    }

    private void SetLightColor(GameObject light, Color color)
    {
        light.GetComponent<Renderer>().material.color = color;
        light.GetComponent<Light>().color = color;
    }
    
    private IEnumerator HitEffect()
    {
        bloodPanel.enabled = true;
        yield return new WaitForSeconds(0.3f);
        bloodPanel.enabled = false;
    }
    
}  

