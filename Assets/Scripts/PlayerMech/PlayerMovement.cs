﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;

    public Camera freeCam;
    public Camera mechCam;


    private void Start()
    {
        freeCam.gameObject.SetActive(false);
        mechCam.gameObject.SetActive(true);

    }
    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        //TODO: another component
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            freeCam.gameObject.SetActive(true);
            mechCam.gameObject.SetActive(false);
        }
        else
        {
            freeCam.gameObject.SetActive(false);
            mechCam.gameObject.SetActive(true);
        }
    }
}
 