﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechtColliderController : MonoBehaviour
{    
    [SerializeField] private string partName;
    [SerializeField] private float meleeDamage;

    public void OnCollisionEnter(Collision collision)
    {        

        if (collision.gameObject.CompareTag("Enemy"))
        {
            partName = collision.contacts[0].thisCollider.name;
            Debug.Log("HIt on "+partName);

            if (collision.gameObject.GetComponent<Enemy>())
            {
                HealthManagerScript.Instance.OnDamage(collision.gameObject.GetComponent<Enemy>().enemyDamage, partName);

            }else if (collision.gameObject.GetComponent<MetalonController>())
            {
                HealthManagerScript.Instance.OnDamage(collision.gameObject.GetComponent<MetalonController>().enemyDamage, partName);
            }


            //Destroy(collision.gameObject);

            CheckIfdamageable(collision.gameObject, 0);
        }
    }

    public void CheckIfdamageable(GameObject collisionObject, float angle)
    {
        IDamageable idamageable = collisionObject.GetComponent<IDamageable>();
        if (idamageable != null)
        {            
            idamageable.Damage(meleeDamage);

        }
    }
}
