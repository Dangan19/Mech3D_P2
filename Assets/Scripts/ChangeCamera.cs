﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class ChangeCamera : MonoBehaviour
{
    [SerializeField] private Camera _mechCamera;
    [SerializeField] private Camera _freeLookCamera;
    [SerializeField] private Transform pilot;
    [SerializeField] private Quaternion pilotRotation;
    [SerializeField] private FirstPersonController mechScript;
    [SerializeField] private PilotCamera freeScript;
    [SerializeField] private Transform reference;


    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            _mechCamera.enabled = false;
            mechScript.enabled = false;

            _freeLookCamera.enabled = true;
            freeScript.enabled = true;

            /*
            if (_mechCamera.enabled)
            {

                _mechCamera.enabled = false;
                mechScript.enabled = false;

                _freeLookCamera.enabled = true;
                freeScript.enabled = true;


            }
            else
            {
                _mechCamera.enabled = true;
                mechScript.enabled = true;
                _freeLookCamera.transform.LookAt(reference);
                _freeLookCamera.enabled = false;
                freeScript.enabled = false;


            }

            _freeLookCamera.transform.LookAt(reference);
            */
        }
        else
        {
            _mechCamera.enabled = true;
            mechScript.enabled = true;
            _freeLookCamera.transform.LookAt(reference);
            _freeLookCamera.enabled = false;
            freeScript.enabled = false;
        }

        _freeLookCamera.transform.LookAt(reference);
    }
}

