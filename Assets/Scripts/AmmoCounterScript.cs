﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoCounterScript : MonoBehaviour
{
    [SerializeField]private GameObject[] blockList;
    [SerializeField]private ProjectileGunTutorial gun;
    [SerializeField] private GameObject ledLight;

    private int maxBullets;
    private int bulletsLeft;
    private Renderer render;

    // Start is called before the first frame update
    void Start()
    {
        
        Debug.Log("MAX BULLETS: "+maxBullets);

        gun.OnFinish += ResetCounter;
        gun.OnReload += ManualReload;


        render = ledLight.GetComponent<Renderer>();

        SetLights(Color.green);
        SetColorBlocks(Color.green);
              
    }  

    // Update is called once per frame
    void Update()
    {
        
        bulletsLeft = gun.GetCurrentBullets();
        maxBullets = gun.GetMagazineSize();
        //Debug.Log(bulletsLeft);

        if (bulletsLeft <= maxBullets * 0.75)
        {
            blockList[3].SetActive(false);

        }
        if (bulletsLeft <= maxBullets * 0.5)
        {
            blockList[2].SetActive(false);
            SetColorBlocks(Color.yellow);            
        }
        if (bulletsLeft <= maxBullets * 0.25)
        {
            blockList[1].SetActive(false);
            SetColorBlocks(Color.red);
        }
        if (bulletsLeft <= maxBullets * 0.01)
        {
            blockList[1].SetActive(false);
            
        }
        if (bulletsLeft == 0 && GameManager.Instance.currentState == GameManager.temperatureStates.overheat)
        {
            blockList[0].SetActive(false);
            SetLights(Color.red);
        }

    }

    void ResetCounter()
    {
        for (int i = 0; i < blockList.Length; i++)
        {
            blockList[i].SetActive(true);
        }

        SetColorBlocks(Color.green);
        SetLights(Color.green);
    }

    void SetLights(Color c)
    {
        render.material.SetColor("_Color", c);
        render.material.SetColor("_EmissionColor", c);
    }

    private void SetColorBlocks(Color c)
    {
        for (int i = 0; i< blockList.Length; i++)
        {
            blockList[i].GetComponent<Renderer>().material.SetColor("_Color", c);
        }
        
    }

    private void ManualReload()
    {
        //bulletsLeft = 0;
        SetLights(Color.red);
    }
}
