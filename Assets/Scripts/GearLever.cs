﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearLever : MonoBehaviour, IClickable
{
    public int leverNum;    
    public bool leverState;
    public AudioClip audioClip;
    private Animator anim;
    private AudioSource audioSource;

    private void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("Click", leverState);
        audioSource = GetComponent<AudioSource>();
    }

    public void OnClick()
    {
        audioSource.clip = audioClip;
        audioSource.Play();

        if (leverState)
        {
            leverState = false;   
        }
        else
        {
            leverState = true; 
        }

        anim.SetBool("Click", leverState);
    }

}
