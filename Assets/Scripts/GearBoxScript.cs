﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public class GearBoxScript : MonoBehaviour
    {
        [SerializeField] private GearLever[] levers;
        [SerializeField] private FirstPersonController fpc;
        [SerializeField] private float[] velocities;
        [SerializeField] private float[] gasConsumption;
        [SerializeField] private AudioClip engineSound;
        private AudioSource audioSource;
        private bool decreased;

        private bool audioPlaying;

        // Start is called before the first frame update
        void Awake()
        {
            levers[0].leverState = true;
            levers[1].leverState = false;
            levers[2].leverState = false;
        }

        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            audioPlaying = false;
            decreased = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (levers[0].leverState && !levers[1].leverState && !levers[2].leverState)
            {
                fpc.SetMovementSpeed(velocities[0]);
                audioSource.volume = 0.15f;
                HealthManagerScript.Instance.SetBurnRate(gasConsumption[0]);

            }
            else if (levers[1].leverState && !levers[2].leverState && !levers[0].leverState)
            {
                fpc.SetMovementSpeed(velocities[1]);
                audioSource.volume = 0.2f;
                HealthManagerScript.Instance.SetBurnRate(gasConsumption[1]);

                if (GameManager.Instance.currentState == GameManager.temperatureStates.increaseTemp || GameManager.Instance.currentState == GameManager.temperatureStates.decreaseTemp)
                {
                    GameManager.Instance.increaseTemp(1 * Time.deltaTime);
                }
            }
            else if (levers[2].leverState && !levers[1].leverState && !levers[0].leverState)
            {
                fpc.SetMovementSpeed(velocities[2]);
                audioSource.volume = 0.25f;
                HealthManagerScript.Instance.SetBurnRate(gasConsumption[2]);
                if (GameManager.Instance.currentState == GameManager.temperatureStates.increaseTemp || GameManager.Instance.currentState == GameManager.temperatureStates.decreaseTemp)
                {
                    GameManager.Instance.increaseTemp(2 * Time.deltaTime);
                }
            }
            else
            {
                fpc.SetMovementSpeed(0);
                audioSource.volume = 0.1f;
            }

            if (GameManager.Instance.currentState == GameManager.temperatureStates.overheat)
            {
                decreased = false;
                if (levers[0].leverState)
                {
                    levers[0].OnClick();
                }
                if (levers[1].leverState)
                {
                    levers[1].OnClick();
                }
                if (levers[2].leverState)
                {
                    levers[2].OnClick();
                }
                fpc.SetMovementSpeed(velocities[3]);
                HealthManagerScript.Instance.SetBurnRate(gasConsumption[0]);
                Debug.Log("SPEED DECREASED");
            }
            else if (GameManager.Instance.currentState == GameManager.temperatureStates.decreaseTemp && !decreased)
            {
                levers[0].OnClick();
                fpc.SetMovementSpeed(velocities[0]);
                audioSource.volume = 0.15f;
                HealthManagerScript.Instance.SetBurnRate(gasConsumption[0]);
                decreased = true;
            }

            if (!audioPlaying)
            {
                audioReady();
            }
        }

        private void audioReady()
        {
            audioSource.clip = engineSound;
            audioSource.Play();
            audioPlaying = true;
        }
    }
}

