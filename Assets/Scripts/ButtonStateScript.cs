﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonStateScript : MonoBehaviour, IClickable
{
    [SerializeField]private int buttonNum;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void OnClick()
    {
        audioSource.Play();
        if (GameManager.Instance.currentState != GameManager.temperatureStates.overheat)
        {
            HealthManagerScript.Instance.StateButtonPressed(buttonNum);
        }
    }
}
