﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreenManager : MonoBehaviour
{
    //Panels
    [SerializeField] private GameObject winPanel;
    [SerializeField] private GameObject losePanel;

    //Text
    [SerializeField] private Text enemiesTxtW;
    [SerializeField] private Text nhulTxtW;
    [SerializeField] private Text enemiesTxtL;
    [SerializeField] private Text nhulTxtL;

    //Stats
    [SerializeField] private int enemiesCount;
    [SerializeField] private int nhullCollected;

    //Music
    [SerializeField] private AudioSource audiosource;
    [SerializeField] private AudioClip victoryMusic;
    [SerializeField] private AudioClip defeatMusic;

    // Start is called before the first frame update
    void Start()
    {
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        enemiesCount = GameManager.Instance.enemiesKilled;

        if (PlayerPrefs.GetInt("resultID") == 0)
        {
            winPanel.SetActive(true);
            enemiesTxtW.text = "Enemies killed: " + enemiesCount;
            nhulTxtW.text = "The motherland is proud!!!";

            audiosource.clip = victoryMusic;
            audiosource.Play();
        }
        if (PlayerPrefs.GetInt("resultID") == 1)
        {
            losePanel.SetActive(true);
            enemiesTxtL.text = "Enemies killed: " + enemiesCount;
            nhulTxtL.text = "You have failed the motherland";

            audiosource.clip = defeatMusic;
            audiosource.Play();
        }
    }
}
