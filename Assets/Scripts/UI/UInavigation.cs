﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UInavigation : MonoBehaviour
{
    private Button b;
    public GameObject hideUI;
    public GameObject showUI;

    // Start is called before the first frame update
    void Start()
    {
        b = GetComponent<Button>();
        b.onClick.AddListener(UIchange);
    }

    private void UIchange()
    {
        hideUI.SetActive(false);
        showUI.SetActive(true);
    }
}
