﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GarageWeapon : MonoBehaviour
{
    [SerializeField] private Image weaponImg;
    [SerializeField] private Text weaponName;
    [SerializeField] private int id;

    private EquipWeapon equipWpn;

    private void Awake()
    {
        equipWpn = FindObjectOfType<EquipWeapon>();
    }

    private void Start()
    {
        weaponName = gameObject.GetComponent<Text>();
        weaponImg = gameObject.GetComponent<Image>();    
    }

    public void CreateWeapon(Weapon weapon)
    {
        weaponImg.sprite = weapon.weaponImg;
        weaponName.text = weapon.weaponName.ToString();
        id = weapon.id;
    }

    public void EquipWpn()
    {
        equipWpn.IncludeEquipment(weaponImg, id);
    }
}
