﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBriefing : MonoBehaviour
{
    public GameObject briefing1;
    public GameObject briefing2;
    public GameObject briefing3;

    // Start is called before the first frame update
    void Start()
    {
       if (PlayerPrefs.GetInt("mapID") == 1)
       {
            briefing2.SetActive(false);
            briefing3.SetActive(false);
        }
       if (PlayerPrefs.GetInt("mapID") == 2)
        {
            briefing1.SetActive(false);
            briefing3.SetActive(false);
        }
        if (PlayerPrefs.GetInt("mapID") == 3)
        {
            briefing2.SetActive(false);
            briefing1.SetActive(false);
        }
    }
}
