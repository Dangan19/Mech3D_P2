﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] private EquipWeapon ew;    
    private GameObject eq;

    private void Start()
    {
        eq = GameObject.Find("Equipment");
    }
    public void GoToScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void UnequipWeapon()
    {
        PlayerPrefs.SetInt("leftId", 2);
        PlayerPrefs.SetInt("rightId", 2);

        foreach (Transform child in eq.transform)
        {
            Destroy(child.gameObject);
        }

        ew.numMinWeaponsEquiped = 1;
    }

    public void GoToMapByID()
    {
        if (PlayerPrefs.GetInt("mapID") == 1)
        {
            SceneManager.LoadScene("FirstContactMap");

        }else if (PlayerPrefs.GetInt("mapID") == 2)
        {
            SceneManager.LoadScene("TundraMission");

        }else if (PlayerPrefs.GetInt("mapID") == 3)
        {
            SceneManager.LoadScene("MapBoss");
        }
        else
        {
            Debug.Log("bad id");
        }
    }
}
