﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarageManager : MonoBehaviour
{
    [SerializeField] GameObject prefabWeapon;
    [SerializeField] int maxWeapon;
    [SerializeField] Weapon[] garageList;
    [SerializeField] List<Weapon> garageProvList;

    private GarageWeapon weapon;

    // Start is called before the first frame update
    void Start()
    {
        garageProvList.AddRange(garageList);

        for (int i = 0; i < maxWeapon; i++)
        {
            GameObject garage = GameObject.Instantiate(prefabWeapon, Vector2.zero, Quaternion.identity, GameObject.FindGameObjectWithTag("Garage").transform);
            //int index = Random.Range(0, garageProvList.Count);
            weapon = garage.GetComponent<GarageWeapon>();
            weapon.CreateWeapon(garageProvList[i]);
        }
    }
}
