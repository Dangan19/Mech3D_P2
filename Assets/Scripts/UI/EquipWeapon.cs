﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipWeapon : MonoBehaviour
{
    [SerializeField] private GameObject weaponEquip;
    public int numMinWeaponsEquiped;
    [SerializeField] private int numMaxWeaponsEquiped;
    private bool isLeft;

    private void Start()
    {
        isLeft = true;
    }

    public void IncludeEquipment(Image weaponIMG, int id)
    {
        if (numMinWeaponsEquiped <= numMaxWeaponsEquiped)
        {
            numMinWeaponsEquiped++;
            GameObject equip = GameObject.Instantiate(weaponEquip, Vector2.zero, Quaternion.identity, GameObject.FindGameObjectWithTag("Equipment").transform);
            Image img = equip.GetComponent<Image>();
            img.sprite = weaponIMG.sprite;

            if (isLeft) {
                PlayerPrefs.SetInt("leftId", id);
                print("left weapon = " + id);
                //GameManager.Instance.leftWeaponId = id;
                isLeft = false;
            }
            else
            {
                PlayerPrefs.SetInt("rightId", id);
                print("right weapon = " + id);
                //GameManager.Instance.rightWeaponId  = id;                
            }

            //GameManager.Instance.EquipWeapons();
        }
    }
}
