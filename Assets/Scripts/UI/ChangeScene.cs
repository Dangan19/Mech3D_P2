﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ChangeScene : MonoBehaviour
{
    [SerializeField] private int sceneID;
    private Button button;
    public string scene;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(GoToScene);
    }

    private void GoToScene()
    {
        if (scene == "ResultScene") 
        {
            RetryLevel();
        } 
        else 
        {
            PlayerPrefs.SetInt("mapID", sceneID);
            SceneManager.LoadScene(scene);
        } 
    }

    private void RetryLevel() 
    {
        if (PlayerPrefs.GetInt("mapID") == 1)
        {
            SceneManager.LoadScene("TestMapScene");
        }
        else if (PlayerPrefs.GetInt("mapID") == 2)
        {
            SceneManager.LoadScene("TundraMission");

        }else if (PlayerPrefs.GetInt("mapID") == 3)
        {
            SceneManager.LoadScene("MapBoss");
        }
    }
}
