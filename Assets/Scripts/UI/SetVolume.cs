﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetVolume : MonoBehaviour
{
    [SerializeField] private string mixername;
    private Slider slider;
    public AudioMixer mixer;

    private void Awake()
    {
        //mixer.SetFloat(mixername, PlayerPrefs.GetFloat(mixername + "Volume"));
        slider = GetComponent<Slider>();
        slider.value = PlayerPrefs.GetFloat(mixername + "Volume");
        mixer.SetFloat(mixername, Mathf.Log10(slider.value) * 20);
        slider.onValueChanged.AddListener(delegate { SetLevel(); });
    }

    public void SetLevel()
    {
        mixer.SetFloat(mixername, Mathf.Log10(slider.value) * 20);
        PlayerPrefs.SetFloat(mixername + "Volume", slider.value);
    }

}
