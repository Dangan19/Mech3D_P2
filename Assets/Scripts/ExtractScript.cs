﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExtractScript : MonoBehaviour
{
    [SerializeField] private int resultScreenID;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && GameManager.Instance.canExtract)
        {
            resultScreenID = 0;
            PlayerPrefs.SetInt("resultID", resultScreenID);
            SceneManager.LoadScene("ResultScene");
        }
    }
}
