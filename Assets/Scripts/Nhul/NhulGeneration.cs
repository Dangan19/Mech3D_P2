﻿using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections;

public class NhulGeneration : MonoBehaviour
{
    public GameObject nhul;
    /*
    Vector3[] positions = { new Vector3 { x = 347, y = 24, z = 371 },
                            new Vector3 { x = 373, y = 18, z = 385 },
                            new Vector3 { x = 385, y = 1, z = 445 },
                            new Vector3 { x = 304, y = 6, z = 457 },
                            new Vector3 { x = 266, y = 3, z = 425 },
                            new Vector3 { x = 256, y = 1, z = 367 },
                            new Vector3 { x = 276, y = 18, z = 310 },
                            new Vector3 { x = 373, y = 18, z = 385 },
                            new Vector3 { x = 228, y = 0, z = 309 },
                            new Vector3 { x = 216, y = 0, z = 301 },
                            new Vector3 { x = 221, y = 1, z = 292 },
                            new Vector3 { x = 204, y = 0, z = 289 },
                            new Vector3 { x = 198, y = 0, z = 276 },
                            new Vector3 { x = 182, y = 0, z = 278 },
                            new Vector3 { x = 143, y = 13, z = 351 },
                            new Vector3 { x = 146, y = 0, z = 316 },
                            new Vector3 { x = 162, y = 2, z = 316 },
                            new Vector3 { x = 151, y = 1, z = 257 },
                            new Vector3 { x = 147, y = 1, z = 228 },
                            new Vector3 { x = 160, y = 1, z = 216 },
                            new Vector3 { x = 171, y = 1, z = 212 },
                            new Vector3 { x = 99, y = 3, z = 190 },
                            new Vector3 { x = 147, y = 0, z = 156 },
                            new Vector3 { x = 150, y = 0, z = 112 },
                            new Vector3 { x = 155, y = 0, z = 83 },
                            new Vector3 { x = 143, y = 3, z = 59 },
                            new Vector3 { x = 113, y = 12, z = 66 },
                            new Vector3 { x = 179, y = 1, z = 86 },
                            new Vector3 { x = 211, y = 1, z = 82 },
                            new Vector3 { x = 206, y = 2, z = 99 },
                            new Vector3 { x = 204, y = 18, z = 385 },
                            new Vector3 { x = 233, y = 1, z = 128 },
                            new Vector3 { x = 233, y = 1, z = 156 },
                            new Vector3 { x = 233, y = 1, z = 175 },
                            new Vector3 { x = 245, y = 0, z = 200 },
                            new Vector3 { x = 264, y = 0, z = 218 },
                            new Vector3 { x = 293, y = 0, z = 186 },
                            new Vector3 { x = 305, y = 0, z = 186 },
                            new Vector3 { x = 320, y = 1, z = 232 },
                            new Vector3 { x = 322, y = 0, z = 204 },
                            new Vector3 { x = 357, y = 1, z = 232 },
                            new Vector3 { x = 377, y = 1, z = 212 },
                            new Vector3 { x = 392, y = 5, z = 202 },
                            new Vector3 { x = 403, y = 3, z = 177 },
                            new Vector3 { x = 400, y = 1, z = 120 },
                            new Vector3 { x = 399, y = 0, z = 101 },
                            new Vector3 { x = 408, y = 1, z = 83 },
                            new Vector3 { x = 427, y = 2, z = 146 },
                            new Vector3 { x = 367, y = 1, z = 327 },
                            new Vector3 { x = 360, y = 1, z = 282 },
                            new Vector3 { x = 315, y = 0, z = 260 },
                            new Vector3 { x = 303, y = 0, z = 248 },
                            new Vector3 { x = 258, y = 1, z = 234 },
                            new Vector3 { x = 223, y = 0, z = 266 },
                            new Vector3 { x = 240, y = 0, z = 200 }
    };
    */
    [SerializeField] private GameObject[] spawnPoints;
    [SerializeField] private int maxNhul;
    [SerializeField] private int minNhul;
    private float max;
    private int nhulNum;
    

    void Start()
    {
        max = 0;
        nhulNum = Random.Range(minNhul, maxNhul);
        //Vector3[] spawnPos = new Vector3[nhulNum];
        StartCoroutine(spawn());
    }

    void FixedUpdate()
    {

    }

    IEnumerator spawn()
    {
        while (max < nhulNum)
        { 
            var rndPos = Random.Range(0, spawnPoints.Length);
            yield return new WaitForSeconds(0.1f);
            var theNewPos = spawnPoints[rndPos];
            GameObject go = GameObject.Instantiate(nhul);
            go.transform.position = theNewPos.transform.position;
            max += 1;
        }
    }
}