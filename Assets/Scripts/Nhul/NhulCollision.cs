﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NhulCollision : MonoBehaviour
{
    public GameObject Mena;

    //Es crea el tipus Contact
    public delegate void Contact();
    //El relacionem amb la funció a disparar.
    public event Contact OnContact;
    public event Contact OffContact;


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Mena"))
        {
            Debug.Log("Mena in range");
            Mena = other.gameObject;

            OnContact();
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Mena"))
        {
            Debug.Log("Mena out of range");
            Mena = null;
            StopCoroutine("onExtract");

            OffContact();
        }
    }

    public void playParticles()
    {
        Mena.GetComponent<NhullParticles>().PlayParticles();
    }

    public void resetLights()
    {
        OffContact();
    }
}
