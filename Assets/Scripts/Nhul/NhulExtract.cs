﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NhulExtract : MonoBehaviour, IClickable
{
    public NhulCollision nhulCollision;
    public CapsuleCollider cd;
    private float shrinkTime;

    public bool extactLeverState;
    private Animator anim;


    public AudioClip leverClip;
    public AudioClip miningClip;

    private AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        shrinkTime = 2.7f;
        anim = GetComponent<Animator>();
        anim.SetBool("Click", extactLeverState);
        audioSource = GetComponent<AudioSource>();

    }

    public void OnClick()
    {
        audioSource.clip = leverClip;
        audioSource.Play();

        if (nhulCollision.Mena != null)
        {
            Debug.Log("Start extract");
            extactLeverState = true;
            StartCoroutine("onExtract");
        }
        else
        { 
            Debug.Log("there's nothing to mine");
            StartCoroutine("nothingToMine");
        }

        anim.SetBool("Click", extactLeverState);
    }

    IEnumerator onExtract()
    {
        Debug.Log("Extracting");        
        nhulCollision.playParticles();
        audioSource.clip = miningClip;
        audioSource.Play();

        yield return new WaitForSeconds(shrinkTime);
        if (GameManager.Instance.currentState == GameManager.temperatureStates.increaseTemp)
        {
            GameManager.Instance.increaseTemp(4 * Time.deltaTime);
        }

        /*
        Vector3 originalScale = nhulCollision.Mena.transform.localScale;
        Vector3 destinationScale = new Vector3(200f, 200f, 200f);

        float currentTime = 0.0f;

        do
        {
            nhulCollision.Mena.transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / shrinkTime);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= shrinkTime);*/

        Debug.Log("Extracted");
        Destroy(nhulCollision.Mena);

        extactLeverState = false;
        nhulCollision.resetLights();
        anim.SetBool("Click", extactLeverState);
    }

    IEnumerator nothingToMine()
    {
        extactLeverState = true;
        anim.SetBool("Click", extactLeverState);
        yield return new WaitForSeconds(0.2f);
        extactLeverState = false;
        anim.SetBool("Click", extactLeverState);
    }
}
