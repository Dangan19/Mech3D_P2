﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PilotCamera : MonoBehaviour
{
    [SerializeField] private MouseLook m_MouseLook;
    [SerializeField] private Camera Camera;   


    void Start()
    {
        m_MouseLook.Init(transform, Camera.transform);

    }

    void Update()
    {
        m_MouseLook.LookRotation(transform, Camera.transform);
       
    }
}
