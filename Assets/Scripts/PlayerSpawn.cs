﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    [SerializeField] private GameObject player;
    
    // Start is called before the first frame update
    void Start()
    {
        int n = Random.Range(0, transform.childCount);
        Vector3 spawnPoint = transform.GetChild(n).transform.position - player.transform.position;
        player.GetComponent<CharacterController>().Move(spawnPoint);
    }
}
