﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtractLightScript : MonoBehaviour
{
    public NhulCollision nhulCollision;
    private Renderer renderer;
    private Light light;

    // Start is called before the first frame update
    void Start()
    {
        renderer = gameObject.GetComponent<Renderer>();
        light = GetComponent<Light>();


        nhulCollision.OnContact += lightOn;
        nhulCollision.OffContact += lightOff;
    }

    private void lightOff()
    {
        renderer.material.SetColor("_Color", Color.red);
        light.color = Color.red;
    }


    private void lightOn()
    {
        renderer.material.SetColor("_Color", Color.green);
        light.color = Color.green;
    }
}
