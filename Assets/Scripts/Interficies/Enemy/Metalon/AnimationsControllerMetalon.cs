﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsControllerMetalon : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetMovingState(bool val)
    {
        _animator.SetBool("Walk Forward", val);
    }

    public void SetRunningState(bool val)
    {
        _animator.SetBool("Run Forward", val);
    }

    public void SetDead()
    {        
        _animator.SetTrigger("Die");
    }
    /*
    public void ClearDead()
    {
        _animator.SetBool(IsDeadHash, false);
        _animator.Play("Idle");
    }
    */
    public void Attack()
    {
        _animator.SetTrigger("Stab Attack");
    }

    public void Hit()
    {
        _animator.SetTrigger("Take Damage");
    }
}
