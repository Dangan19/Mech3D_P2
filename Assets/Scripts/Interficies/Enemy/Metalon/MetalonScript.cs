﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MetalonScript : MonoBehaviour
{
    public Transform player;
    private NavMeshAgent enemy;
    public bool bDistance;
    protected bool canShoot = false;
    private NavMeshPath path;
    private bool isStunned = false;
    private bool firstCheck;
    private Vector3 dashTransform;
    private AudioSource audioSource;
    private MetalonController mt;
    private float maxHealth;

    [SerializeField] protected AnimationsControllerMetalon ac;
    [SerializeField] private ParticleSystem smokeTrail;
    [SerializeField] public float speed;   
    [SerializeField] public float speedBoost;    
    [SerializeField] private float maxDistance;
    [SerializeField] private float dashDistance;
    [SerializeField] private float stopDistance;
    [SerializeField] private float stuntTime;



    // Use this for initialization
    public virtual void Start()
    {

        enemy = GetComponent<NavMeshAgent>();
        //player = GameObject.Find("Player").GetComponent<Transform>();

        player = GameManager.Instance.player.transform;
        enemy.SetDestination(player.transform.position);
        path = new NavMeshPath();

        enemy.speed = speed;
        enemy.stoppingDistance = stopDistance;

        firstCheck = true;

        audioSource = gameObject.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    public virtual void Update()
    {        

        if (Vector3.Distance(player.position, gameObject.transform.position) <= maxDistance)
        {

            bool bDistance = enemy.remainingDistance >= enemy.stoppingDistance;
            Debug.Log(bDistance);

            if (!isStunned) {

                Move(bDistance);
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ac.Attack();
        }
    }

    protected void Move(bool bDistance)
    {
        if (!bDistance)
        {
            enemy.isStopped = true;
            ac.SetMovingState(false);
            ac.SetRunningState(false);
            Debug.Log("Enemy arrive at player");

            StartCoroutine(Stun());

        }
        else
        {

            if (Vector3.Distance(player.position, gameObject.transform.position) <= dashDistance)
            {
                Debug.Log("First check is " + firstCheck);

                if (firstCheck)
                {
                    GameObject trail = Instantiate(smokeTrail, gameObject.transform.position, Quaternion.identity).gameObject;
                    trail.transform.parent = gameObject.transform;
                    dashTransform = player.position;
                    firstCheck = false;
                    audioSource.Play();
                }
                else
                {
                    
                    enemy.speed = speed + speedBoost;

                    ac.SetMovingState(false);
                    ac.SetRunningState(true);

                    enemy.isStopped = false;
                    enemy.SetDestination(dashTransform);
                }

                
            }
            else
            {
                enemy.speed = speed;

                enemy.isStopped = false;
                enemy.SetDestination(player.position);
                ac.SetMovingState(true);
                ac.SetRunningState(false);
            }
        }
    }

    IEnumerator Stun()
    {
        isStunned = true;
        Debug.Log("is stunned");
        yield return new WaitForSeconds(stuntTime);
        isStunned = false;
        enemy.isStopped = false;
        enemy.SetDestination(player.position);
        firstCheck = true;
    }
}
