﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MetalonController : MonoBehaviour, IDamageable
{
    public float enemyHealth;
    public float enemyDamage;
    private float maxHealth;
    public AnimationsControllerMetalon ac;
    private Collider collider;
    private NavMeshAgent enemy;

    [SerializeField]private GameObject explosion;
    [SerializeField] public MetalonScript ms;
    [SerializeField] private float speedBuff;
    [SerializeField] private float speedBoostBuff;


    void Start()
    {
        collider = gameObject.GetComponent<Collider>();
        maxHealth = enemyHealth;
        ms = GetComponent<MetalonScript>();
    }

    void Update()
    {
        if (enemyHealth < maxHealth * 0.5)
        {
            ms.speed = speedBuff;
            ms.speedBoost = speedBoostBuff;
        }
    }

    public void Damage(float damage)
    {
        ac.Hit();
        enemyHealth -= damage;

        if (enemyHealth <= 0)
        {
            collider.enabled = false;
            //enemy.isStopped = true;
            StartCoroutine(Death());
        }
    }

    public IEnumerator Death()
    {
        ac.SetDead();
        yield return new WaitForSeconds(0.2f);
        Instantiate(explosion, new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y + 2, gameObject.transform.position.z) , Quaternion.identity);
        GameManager.Instance.canExtract = true;
        Destroy(gameObject);
    }
}
