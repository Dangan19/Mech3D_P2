﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerScript : MonoBehaviour
{
    private Transform playerPos;
    [SerializeField] private AnimationsController ac;
    [SerializeField] private float speed;
    [SerializeField] private float maxDistance;

    //private bool playerReached;

    void Start()
    {
        //playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        playerPos = GameManager.Instance.player.transform;
        //playerReached = false;
    }

    void FixedUpdate()
    {
        // rango de detecció
        if (Vector3.Distance(playerPos.position, gameObject.transform.position) <= maxDistance)
        {
            FollowPlayer();
        }
        else
        {
            ac.SetMovingState(false);
        }
    }

    private void FollowPlayer()
    {
        Vector3 position = Vector3.MoveTowards(transform.position, playerPos.position, speed * Time.fixedDeltaTime);
        ac.SetMovingState(true);
        transform.position = position;
        transform.LookAt(playerPos);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ac.Attack();
        }
    }
}