﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnemy : MonoBehaviour
{
    public float enemyHealth;
    public float enemyDamage;

    public IEnumerator Death()
    {
        yield return new WaitForSeconds(2f);
        GameManager.Instance.enemiesKilled++;
        Destroy(gameObject);
    }
}
