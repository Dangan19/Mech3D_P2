﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootSpiderNavMesh :  FollowNavMesh
{
    [SerializeField] private Rigidbody bullet;
    [SerializeField] private float bulletSpeed;

    [SerializeField] private Transform firePoint;
    private Vector3 lastPosition = Vector3.zero;
    private Vector3 shootVector;
    private Vector3 angle;
    private Quaternion targetRotation;

    private bool canFire = true;
    private float fireCooldown;

    [SerializeField] private float startFireCooldown;
    [SerializeField] private float spread;

    public AudioClip wepSoundClip;
    private AudioSource audioSource;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        canFire = true;
        fireCooldown = 0;
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = wepSoundClip;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        
        if (canShoot)
        {
            Debug.Log("Ready to shoot");

            if (gameObject.GetComponent<EnemyMinion>().enemyHealth > 0)
            {
                Debug.Log("piu araña pot dispara");
                transform.LookAt(player);
                var rand = Random.Range(-spread, spread);
                //implement random ranges on playerPos.transform x, y, z;
                lastPosition = new Vector3(player.transform.position.x + rand, (player.transform.position.y + rand), player.transform.position.z);
                shootVector = lastPosition - transform.position;
                angle = Quaternion.Euler(0, 180, 0) * shootVector;
                targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: angle);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 2 * Time.fixedDeltaTime);

                Shoot();
            }

            
        }

    }

    private void FixedUpdate()
    {
        if (fireCooldown <= 0)
        {
            canFire = true;
        }
        else
        {
            fireCooldown -= Time.fixedDeltaTime;
        }
    }

    private void Shoot()
    {
        if (canFire)
        {
            ac.Attack();
            audioSource.Play();
            Rigidbody instance = Instantiate(bullet, firePoint.position, firePoint.rotation);
            shootVector.Normalize();
            instance.velocity = shootVector * bulletSpeed;
            fireCooldown = startFireCooldown;
            canFire = false;
        }
    }
}
