﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMinion : Enemy, IDamageable
{
    private Collider collider;
    void Start()
    {
        collider = gameObject.GetComponent<Collider>();
    }

    public void Damage(float damage)
    {
        Debug.Log("damage: " + damage);
        Debug.Log("health left: " + enemyHealth);
        ac.Hit();
        enemyHealth-=damage;

        if (enemyHealth <= 0)
        {
            collider.enabled = false;
            StartCoroutine(Death());
        }
    }
}
