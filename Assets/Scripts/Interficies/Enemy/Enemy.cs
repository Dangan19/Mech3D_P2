﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float enemyHealth;
    public float enemyDamage;
    public AnimationsController ac;

    public IEnumerator Death()
    {
        ac.ClearDead();
        ac.SetDead();
        yield return new WaitForSeconds(2f);
        GameManager.Instance.enemiesKilled ++;
        Destroy(gameObject);
    }

    public IEnumerator CocoonDeath(ParticleSystem ps)
    {
        ps.Play();
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
