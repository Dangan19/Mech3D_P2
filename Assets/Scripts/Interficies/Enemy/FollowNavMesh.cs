﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowNavMesh : MonoBehaviour
{
    public Transform player;
    private NavMeshAgent enemy;
    public bool bDistance;
    protected bool canShoot = false;
    private NavMeshPath path;

    [SerializeField] protected AnimationsController ac;
    [SerializeField] private float speed;
    [SerializeField] private float maxDistance;
    [SerializeField] private float stopDistance;


    // Use this for initialization
    public virtual void Start()
    {
        enemy = GetComponent<NavMeshAgent>();        
        player = GameObject.Find("Player").GetComponent<Transform>();
        enemy.SetDestination(player.transform.position);
        path = new NavMeshPath();

        enemy.speed = speed;
        enemy.stoppingDistance = stopDistance;        
        
    }

    // Update is called once per frame
   public virtual void Update()
    {
        if (Vector3.Distance(player.position, gameObject.transform.position) <= maxDistance)
        {            

            bool bDistance = enemy.remainingDistance >= enemy.stoppingDistance;
            Debug.Log(bDistance);

            Move(bDistance);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ac.Attack();
        }
    }

    protected void Move(bool bDistance)
    {
        if (!bDistance)
        {
            enemy.isStopped = true;
            ac.SetMovingState(false);
            Debug.Log("Enemy arrive at player");
            canShoot = true;
        }
        else
        {
            canShoot = false;
            enemy.isStopped = false;
            enemy.SetDestination(player.position);
            //NavMesh.CalculatePath(transform.position, player.position, NavMesh.AllAreas, path);
            //enemy.SetPath(path);
            ac.SetMovingState(true);
        }
    }
}
