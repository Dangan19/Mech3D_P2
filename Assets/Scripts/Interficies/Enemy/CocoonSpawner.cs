﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CocoonSpawner : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private GameObject currentEnemy;
    [SerializeField] private float spawnRate;
    [SerializeField] private int maxEnemies;
    [SerializeField] private float spawnRange;
    [SerializeField] private float maxDistance;

    private Vector3 positionSpawn;
    private List<GameObject> enemyList = new List<GameObject>();
    private int numEnemies;
    private IEnumerator coroutine;
    private Transform playerPos;
    private bool playerInRange = false;

    private float currentTime;

    // Start is called before the first frame update
    void Start()
    {
        //SpawnEnemy();

        //coroutine = Spawner(spawnRate);
        playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

        //StartCoroutine(coroutine);
        currentTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (numEnemies < 5)
        {
            foreach (GameObject index in enemyList)
            {
                if(index == null)
                {
                    Debug.Log(index.activeSelf);
                    numEnemies--;
                }
                else
                {
                    numEnemies++;
                }
                
            }

            Debug.Log("total enemies of the cacoon "+numEnemies);
        }
        */

        if (Vector3.Distance(playerPos.position, gameObject.transform.position) <= maxDistance)
        {
            playerInRange = true;
        }
        else
        {
            playerInRange = false;

        }

        if (playerInRange)
        {
            currentTime += Time.deltaTime;

            if (currentTime >= spawnRate)
            {
                 SpawnEnemy();                
            }
        }
    }

    private void SpawnEnemy()
    {
        //CheckSpidersNum;
        positionSpawn = new Vector3(Random.Range(0, spawnRange) + transform.position.x, Random.Range(1, spawnRange) + transform.position.y, transform.position.z);
        currentEnemy = Instantiate(enemy, positionSpawn, Quaternion.identity);
        enemyList.Add(currentEnemy);

        currentTime = 0;
    }

    private void CheckSpidersNum()
    { 
        foreach (GameObject index in enemyList)
        {


            if (index == null)
            {
                Debug.Log("index active: "+index.activeSelf);
                numEnemies--;
            }
            else
            {
                numEnemies++;
            }
        }

        /*
        private IEnumerator Spawner(float waitTime)
        {
            while (playerInRange)
            {
                yield return new WaitForSeconds(waitTime);
                if (numEnemies < maxEnemies)
                {
                    SpawnEnemy();
                }
            }
        }
        */
    }

}
