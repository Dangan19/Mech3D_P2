﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockCamFPS : MonoBehaviour
{
    [SerializeField] private int fps;
    private Camera cam;
    private float elapsed;

    void Start()
    {
        cam = gameObject.GetComponent<Camera>();
        cam.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.unscaledDeltaTime;
        if (elapsed > 1f/fps)
        {
            elapsed = 0;
            StartCoroutine(camDisable());
            cam.Render();
        }

    }

    IEnumerator camDisable()
    {
        yield return new WaitForSeconds(1.5f);
        gameObject.SetActive(false);
    }
}
