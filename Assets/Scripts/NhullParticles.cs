﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NhullParticles : MonoBehaviour
{
    [SerializeField] private ParticleSystem ps;
    [SerializeField] private Animator an;

    public void PlayParticles() 
    {
        ps.Play();
        an.SetBool("mining", true);
    }
}
