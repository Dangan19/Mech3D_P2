﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseMenuScript : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    public GameObject player;
    public ProjectileGunTutorial leftgunScript;
    public ProjectileGunTutorial rightgunScript;
    public ChangeCamera ccScript;

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
           if(GameIsPaused)
            {
                GameIsPaused = false;
            }
            else
            {
                GameIsPaused = true;
            }
        }

        if (!GameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    void Resume()
    {
        pauseMenuUI.SetActive(false);
        //Time.timeScale = 1f;
        //GameIsPaused = false;
        player.GetComponent<FirstPersonController>().enabled = true;
        leftgunScript.enabled = true;
        rightgunScript.enabled = true;
        ccScript.enabled = true;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        //Time.timeScale = 0f;
        //GameIsPaused = true;
        player.GetComponent<FirstPersonController>().enabled = false;
        leftgunScript.enabled = false;
        rightgunScript.enabled = false;
        ccScript.enabled = false;
    }
}
