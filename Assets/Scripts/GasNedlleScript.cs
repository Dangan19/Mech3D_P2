﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasNedlleScript : MonoBehaviour
{
    private Transform nt;
    private const float MAX_GAS_ANGLE = 85;
    private const float ZERO_GAS_ANGLE = -80;

    private void Start()
    {
        nt = gameObject.GetComponent<Transform>();
    }
    // Update is called once per frame
    void Update()
    {
        nt.localEulerAngles = new Vector3(0, 0, Mathf.Lerp(ZERO_GAS_ANGLE, MAX_GAS_ANGLE, HealthManagerScript.Instance.current / HealthManagerScript.Instance.max));
    }
 
}

