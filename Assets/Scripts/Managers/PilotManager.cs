﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PilotManager : MonoBehaviour
{

    [SerializeField] Camera cam;
    [SerializeField] Image defaultCursor;
    private Color colorDefault;
    private Color colorHightlight;
    public RaycastHit hitData;
    private ThrottleControllerScript th;

    void Awake()
    {
        colorDefault = Color.white;
        colorHightlight = Color.green;
    }

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();        
    }

    // Update is called once per frame
    void Update()
    {


        Vector3 fwd = cam.transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(cam.transform.position, fwd * 5, Color.green);

        if (Physics.Raycast(cam.transform.position, fwd, out hitData))
        {            
            IClickable iclick = hitData.collider.gameObject.GetComponent<IClickable>();

            if (iclick != null )
            {
                defaultCursor.color = colorHightlight;
                //Debug.Log("Interactable object");
                if (Input.GetKeyDown(KeyCode.F)) {
                    iclick.OnClick();
                }
            }
            else
            {
                defaultCursor.color = colorDefault;
            }

            
        }

        
    }
    
}
