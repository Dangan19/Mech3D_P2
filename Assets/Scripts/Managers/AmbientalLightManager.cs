﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientalLightManager : MonoBehaviour
{
    public Material daySky;
    public Material nightSky;
    public Light dirLight;

    [SerializeField] private bool forceNight;
    
    
    // Start is called before the first frame update
    void Start()
    {
        var rndSky = Random.Range(1, 4);

        if (forceNight)
        {
            rndSky = 3;
        }

        if (rndSky == 1 || rndSky == 2)
        {
            PlayerPrefs.SetInt("daynightTime", rndSky);
            RenderSettings.skybox = daySky;
            RenderSettings.fogColor = Color.grey;
            RenderSettings.fogDensity = 0.004F;
            
            dirLight.color = Color.white;
        } 
        else if (rndSky == 3)
        {
            PlayerPrefs.SetInt("daynightTime", rndSky);
            RenderSettings.skybox = nightSky;
            RenderSettings.fogColor = Color.black;
            RenderSettings.fogDensity = 0.025F;
            
            dirLight.color = Color.black;
        }
    }
}
