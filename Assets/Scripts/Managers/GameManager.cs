﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    private GameObject[] nhullList;
    public bool canExtract;

    public int enemiesKilled;

    public int leftWeaponId = 1;
    public int rightWeaponId = 2;

    public Weapon leftWeapon;
    public Weapon rightWeapon;

    public Weapon[] weaponList;

    public GameObject player;

    public int nhulNum;

    //Temperature
    [SerializeField] public temperatureStates currentState;
    [SerializeField] private HealthManagerScript hms;
    public float temperature;

    public bool tempIncrease;
    private float tempDecrease;
    private float tempDecreaseOT;
    [SerializeField]private float overheatTime;
    public float maxTemperature;
    public float minTemperature;
    [SerializeField] private float maxTempDay;
    [SerializeField] private float maxTempNight;
    [SerializeField] private float minTempDay;
    [SerializeField] private float minTempNight;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;

        canExtract = false;
        nhulNum = 10;
        EquipWeapons();
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        hms = FindObjectOfType<HealthManagerScript>();

        if (PlayerPrefs.GetInt("daynightTime") == 1 || PlayerPrefs.GetInt("daynightTime") == 2)
        {
            maxTemperature = maxTempDay;
            minTemperature = minTempDay;
        }
        else if (PlayerPrefs.GetInt("daynightTime") == 3)
        {
            maxTemperature = maxTempNight;
            minTemperature = minTempNight;
        }

        currentState = temperatureStates.increaseTemp;
        tempIncrease = false;
        overheatTime = 5;
        tempDecrease = 2;
        tempDecreaseOT = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(nhulCount());

        if (nhulNum <= 0)
        {
            Debug.Log("can extract " + canExtract);
            canExtract = true;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("StartScreen");
        }
        tempFase();

        Debug.Log(temperature);
    }

    //private int countNhul()
    //{
    //   nhullList = GameObject.FindGameObjectsWithTag("Mena");
    //   Debug.Log("nhul list" + nhullList.Length);
    //   return nhullList.Length;
    //}
    public enum temperatureStates
    {
        increaseTemp, overheat, decreaseTemp
    }

    void tempFase()
    {

        switch (currentState)
        {
            case temperatureStates.increaseTemp:
                temperature = Mathf.Clamp(temperature, 0, maxTemperature);
                if (temperature >= maxTemperature)
                {
                    currentState = temperatureStates.overheat;
                }
                if (temperature >= 0 && !tempIncrease)
                {
                    temperature -= tempDecrease * Time.deltaTime;
                }
                else
                {
                    temperature -= tempDecreaseOT * Time.deltaTime;
                }
                break;
            case temperatureStates.overheat:
                StartCoroutine(overheat());
                break;
            case temperatureStates.decreaseTemp:
                temperature -= tempDecrease * Time.deltaTime;
                if (temperature <= minTemperature)
                {
                    currentState = temperatureStates.increaseTemp;
                }
                break;
        }

    }

    public void increaseTemp(float temp)
    {
        temperature += temp;
        tempIncrease = true;
    }

    IEnumerator overheat()
    {
        hms.StateButtonPressed(0);
        yield return new WaitForSeconds(overheatTime);
        tempIncrease = false;
        currentState = temperatureStates.increaseTemp;
    }

    IEnumerator nhulCount()
    {
        yield return new WaitForSeconds(5f);
        nhullList = GameObject.FindGameObjectsWithTag("Mena");
        nhulNum = nhullList.Length;
        //Debug.Log("nhul list" + nhullList.Length);
    }
    
    private Weapon TranformId(int id)
    {
        Weapon sc;

        switch (id)
        {
            case 1:
                sc = weaponList[0];
                break;
            case 2:
                sc = weaponList[1];
                break;
            case 3:
                sc = weaponList[2];
                break;
            case 4:
                sc = weaponList[3];
                break;
            case 5:
                sc = weaponList[4];
                break;
            case 6:
                sc = weaponList[5];
                break;
            default:
                sc = weaponList[0];
                break;
        }
        return sc;
    }
    
    
    public void EquipWeapons()
    {
        leftWeapon = TranformId(PlayerPrefs.GetInt("leftId"));
        rightWeapon = TranformId(PlayerPrefs.GetInt("rightId")); 
    }
    
    
}
