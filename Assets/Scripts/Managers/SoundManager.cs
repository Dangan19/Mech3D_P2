﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    /*
    private static SoundManager _instance = null;
    public AudioSource EffectsAudioSource;
    public AudioSource MusicAudioSource;
    public AudioSource AmbientAudioSource;

    public static SoundManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            //First run, set the instance
            _instance = this;
            DontDestroyOnLoad(gameObject);

        }
        else if (_instance != this)
        {
            //Instance is not the same as the one we have, destroy old one, and reset to newest one
            Destroy(_instance.gameObject);
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        EffectsAudioSource.volume = PlayerPrefs.GetFloat(EffectsAudioSource.name + "volume");
        MusicAudioSource.volume = PlayerPrefs.GetFloat(MusicAudioSource.name + "volume");
        AmbientAudioSource.volume = PlayerPrefs.GetFloat(AmbientAudioSource.name + "volume");
        EffectsAudioSource.mute = PlayerPrefs.GetInt(EffectsAudioSource.name + "mute") == 1;
        MusicAudioSource.mute = PlayerPrefs.GetInt(MusicAudioSource.name + "mute") == 1;
        AmbientAudioSource.mute = PlayerPrefs.GetInt(AmbientAudioSource.name + "mute") == 1;
    }*/
    [SerializeField] string volumeParameter = "MasterVolume";
    [SerializeField] AudioMixer mixer;
    [SerializeField] Slider slider;
    [SerializeField] float multiplier = 30f;
    [SerializeField] private Toggle toggle;
    private bool disableToggle;

    /*
    private void Awake()
    {
        slider.onValueChanged.AddListener(HandleSliderValueChanged);
        toggle.onValueChanged.AddListener(HandleToggleValueChanged);
    }
    */

    private void Start()
    {
        //slider.value = PlayerPrefs.GetFloat(volumeParameter, slider.value);
    }

    private void OnDisable()
    {
        PlayerPrefs.SetFloat(volumeParameter, slider.value);
    }

    private void HandleSliderValueChanged(float value)
    {
        mixer.SetFloat(volumeParameter, Mathf.Log10(value) * multiplier);
        disableToggle = true;
        toggle.isOn = slider.value > slider.minValue;
        disableToggle = false;
    }

    private void HandleToggleValueChanged(bool enableSound)
    {
        if (disableToggle)
        {
            return;
        }
        if (enableSound)
        {
            slider.value = slider.maxValue;
        } 
        else
        {
            slider.value = slider.minValue;
        }
    }
}
