﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiOverHeat : MonoBehaviour, IClickable
{
    private AudioSource audioSourceClick;
    [SerializeField] private GameObject[] blockList;
    private int maxUses;
    private int uses;
    [SerializeField] private AudioSource audioSourceSteam;
    [SerializeField] private ParticleSystem ps;
    private Renderer render;


    // Start is called before the first frame update
    void Start()
    {
        audioSourceClick = gameObject.GetComponent<AudioSource>();
        uses = 0;
        maxUses = 4;
        SetColorBlocks(Color.green);
    }

    void Update()
    {
        if (uses == 1)
        {
            blockList[3].SetActive(false);

        }
        if (uses  == 2)
        {
            blockList[2].SetActive(false);
        }
        if (uses == 3)
        {
            blockList[1].SetActive(false);
        }
        if (uses == 4)
        {
            blockList[0].SetActive(false);
        }
    }

    public void OnClick()
    {
        audioSourceClick.Play();
        if (!GameManager.Instance.tempIncrease && (GameManager.Instance.currentState != GameManager.temperatureStates.overheat) && uses < maxUses)
        {
            uses++;
            GameManager.Instance.temperature = GameManager.Instance.minTemperature;  
            ps.Play();
            audioSourceSteam.Play();
        }
    }

    void SetLights(Color c)
    {
        render.material.SetColor("_Color", c);
        render.material.SetColor("_EmissionColor", c);
    }

    private void SetColorBlocks(Color c)
    {
        for (int i = 0; i < blockList.Length; i++)
        {
            blockList[i].GetComponent<Renderer>().material.SetColor("_Color", c);
        }

    }
}
