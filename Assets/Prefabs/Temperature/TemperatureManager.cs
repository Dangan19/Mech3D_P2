﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperatureManager : MonoBehaviour
{
    private Transform nt;
    [SerializeField] private ParticleSystem ps;
    [SerializeField] Light cockpitLight;
    [SerializeField] Light fireLight;
    private bool effects;
    [SerializeField] private AudioSource audioSourceOH;
    private const float maxTempAngle = 60;
    private const float lowTempAngle = -55;
    // Start is called before the first frame update
    void Start()
    {
        nt = gameObject.GetComponent<Transform>();
        effects = false;
        fireLight.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        nt.localEulerAngles = new Vector3(0, 0, Mathf.Lerp(lowTempAngle, maxTempAngle, GameManager.Instance.temperature / GameManager.Instance.maxTemperature));
        if (GameManager.Instance.currentState == GameManager.temperatureStates.overheat)
        {
            OverHeatEffect();
        }
        else
        {
            ps.Stop();
            if (effects == true)
            {
                effects = false;
                audioSourceOH.Stop();
                fireLight.enabled = false;
                cockpitLight.enabled = !cockpitLight.enabled;
            }
        }
    }

    void OverHeatEffect()
    {
        ps.Play();
        if (effects == false)
        {
            effects = true;
            audioSourceOH.Play();
            fireLight.enabled = true;
            cockpitLight.enabled = !cockpitLight.enabled;
        }
    }
}
